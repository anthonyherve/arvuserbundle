# BlogBundle

This bundle is in development.

## Installation

Add this line to your composer.json:

```php
"arv/userbundle":"0.0.1"
```

Add these lines to your AppKernel.php:

```php
new ARV\UserBundle\ARVUserBundle(),
new FOS\UserBundle\FOSUserBundle(),
# For dev
$bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
```

Add these lines to your config.yml:

```yaml
# FOSUserBundle
fos_user:
    db_driver:          orm
    firewall_name:      main
    use_listener:       false
    user_class:         ARV\BlogBundle\Entity\User
```

Uncomment this line in your config.yml to make translator enabled:

```yaml
framework:
    translator:      { fallback: "%locale%" }
```

Add these lines to your security.yml:

```yaml
security:
    providers:
        fos_userbundle:
            id: fos_user.user_provider.username
    encoders:
        FOS\UserBundle\Model\UserInterface: sha512
    firewalls:
        main:
            pattern: ^/
            form_login:
                provider: fos_userbundle
                csrf_provider: form.csrf_provider
            logout:       true
            anonymous:    true
    access_control:
        - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/, role: ROLE_ADMIN }
    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: ROLE_ADMIN
```