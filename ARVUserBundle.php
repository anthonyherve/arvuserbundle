<?php

namespace ARV\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ARVUserBundle extends Bundle
{

    public function getParent()
    {
        return 'FOSUserBundle';
    }

}
