<?php
namespace ARV\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setUsername("admin");
        $user->setPlainPassword("admin");
        $userManager->updatePassword($user);
        $user->setEmail("admin@admin.fr");
        $user->setEnabled(true);
        $user->addRole('ROLE_ADMIN');
        $userManager->updateCanonicalFields($user);
        $manager->persist($user);

        $user2 = $userManager->createUser();
        $user2->setUsername("user");
        $user2->setPlainPassword("user");
        $userManager->updatePassword($user2);
        $user2->setEmail("user@user.fr");
        $user2->setEnabled(true);
        $user2->addRole('ROLE_USER');
        $userManager->updateCanonicalFields($user2);
        $manager->persist($user2);

        $manager->flush();
    }

}