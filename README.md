# ARVUserBundle

**THIS BUNDLE IS IN DEVELOPMENT.**

This UserBundle extends FOSUserBundle and provides some extra features.

## Documentation

All documentation is stored in the `Resources/doc/index.md` file in this bundle:

[Read the Documentation](Resources/doc/index.md)
